package com.digitalml.service;

import static spark.Spark.*;
import spark.*;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;

import static net.logstash.logback.argument.StructuredArguments.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ConcurrentHashMap;
import java.util.Map;

public class RuntimeExampleV2Routing {

    private static final Logger logger = LoggerFactory.getLogger("runtimeexamplev2:1-2");

    public static void main(String[] args) {
    
        port(4567);
    
        get("/ping", (req, res) -> {
            return "pong";
        });
        
        get("/halt", (request, response) -> {
			stop();
			response.status(202);
			return "";
		});
		
        // Handle timings
        
        Map<Object, Long> timings = new ConcurrentHashMap<>();
        
        before(new Filter() {
        	@Override
        	public void handle(Request request, Response response) throws Exception {
        		timings.put(request, System.nanoTime());
        	}
        });
        
        after(new Filter() {
        	@Override
        	public void handle(Request request, Response response) throws Exception {
        		long start = timings.remove(request);
        		long end =  System.nanoTime();
        		logger.info("log message {} {} {} {} ns", value("apiname", "runtimeexamplev2"), value("apiversion", "1-2"), value("apipath", request.pathInfo()), value("response-timing", (end-start)));
        	}
        });
        
        afterAfter(new Filter() {
        	@Override
        	public void handle(Request request, Response response) throws Exception {
        		timings.remove(request);
        	}
        });

        get("/something", (req, res) -> {
        
            StringBuffer sb = new StringBuffer();

            sb.append("NewElement = " + req.queryParams("NewElement"));
            sb.append("NewElement2 = " + req.queryParams("NewElement2"));

            return "Get Something " + sb.toString();
        });
        get("/else", (req, res) -> {
        
            StringBuffer sb = new StringBuffer();

            sb.append("Request = " + req.queryParams("Request"));

            return "Something else " + sb.toString();
        });
    }

}